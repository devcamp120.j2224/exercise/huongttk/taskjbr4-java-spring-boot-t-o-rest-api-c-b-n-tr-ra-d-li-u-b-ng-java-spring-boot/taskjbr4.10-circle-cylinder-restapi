package com.devcamp.jbr40.circlecylinderapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleCylinderController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getAreaDouble(@RequestParam(value="radius", defaultValue = "1") double radius){
        Circle circle = new Circle(radius);
        return circle.getAre();
    }
    @GetMapping("/cylinder-volume")
    public double getVol(@RequestParam(value="radius", defaultValue = "1") double radius,
                         @RequestParam(value="height", defaultValue = "1") double height){
        Cylinder cylinder = new Cylinder(radius,height);
        return cylinder.getVolume();
        
    }
}
